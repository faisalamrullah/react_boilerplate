import '../assets/style/main.scss'
import Home from './pages/home/homeComponent'

export const App = () => (
  <div>
    <Home />
  </div>
)

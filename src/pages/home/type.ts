export interface Universities {
  alpha_two_code: string
  country: string
  domains: [index: string]
  name: string
  web_pages: [index: string]
}

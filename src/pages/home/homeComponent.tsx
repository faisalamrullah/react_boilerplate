import React, { useEffect, useState } from 'react'
import { getUniversitiesList } from '../../services/home.api'
import { Universities } from './type'
import Loading from '../../components/loading/loading'

const Home = () => {
  const [listUniversities, setListUniversities] = useState<
    Universities[] | null
  >(null)

  useEffect(() => {
    const fetchUniversities = async () => {
      const response = await getUniversitiesList('indonesia')
      setListUniversities(response)
    }
    fetchUniversities()
  }, [])

  if (!listUniversities) return <Loading />
  return (
    <div>
      {listUniversities.map((item, index) => (
        <div key={index}>{item.name}</div>
      ))}
    </div>
  )
}

export default Home

import Api from '../common/api'
import { Universities } from '../pages/home/type'

const getUniversitiesList = async (country: string) => {
  const response = await Api.get<Universities[]>(`/search?country=${country}`)
  return response.data
}

export { getUniversitiesList }
